terraform {
  backend "s3" {
    bucket = "terraform-infra-python-post"
    key    = "infra/terraform.tfstate"
    region = "us-east-1"
  }
}
