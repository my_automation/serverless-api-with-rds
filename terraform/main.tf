resource "aws_db_instance" "default" {
  allocated_storage      = 20
  storage_type           = "gp2"
  engine                 = "postgres"
  engine_version         = "10.18"
  instance_class         = "db.t2.micro"
  name                   = var.name
  username               = var.user
  password               = random_string.password.result
  #parameter_group_name   = "postgres10"
  multi_az               = false
  publicly_accessible    = true
  skip_final_snapshot    = true
  db_subnet_group_name   = module.vpc.database_subnet_group
  vpc_security_group_ids = [aws_security_group.main.id]
}
