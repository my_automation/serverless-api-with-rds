
Steps to perform

Prerequisites:

Need to install terrafrom, npm and serverless (npm install serverless)

Clone repo

```
cd terraform
terraform init
terrafrom apply
```

```
cd ../serverless
serverless deploy
serverless info --verbose | grep ServiceEndpoint | sed s/ServiceEndpoint\:\ //g # Gets Endpoint URL
```

